package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PyramidBuilder {
	private static LinkedList<Integer> pyramidList;

	/**
	 * Builds a pyramid with sorted values (with minumum value at the top line and
	 * maximum at the bottom, from left to right). All vacant positions in the array
	 * are zeros.
	 *
	 * @param inputNumbers to be used in the pyramid
	 * @return 2d array with pyramid inside
	 * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build
	 *                with given input
	 */
	public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

		if (inputNumbers.contains(null))
			throw new CannotBuildPyramidException();

		// prepare LinkedList for more quick pick up
		try {
			pyramidList = new LinkedList<>(inputNumbers);

			// sorting our list
			Collections.sort(pyramidList);

			//create Pyr and fill it with '0'
			int[][] pyramid = createPyramid(pyramidList.size());
			
			// fill Pyr with correct numbers from list
			pyramid = fillPyramid(pyramid);
			
			return pyramid;
			
		} catch (OutOfMemoryError e) {
			throw new CannotBuildPyramidException();
		}
	}

	private static int[][] fillPyramid(int[][] pyramid) {
		int w = pyramid[0].length;
		for (int i = 0; i < pyramid.length; i++) {
			
			//fill 1st number in row
			pyramid[i][w / 2 - i] = pyramidList.poll();
			
			//fill other numbers in row
			for (int j = 0; j < i; j++) {
				pyramid[i][w / 2 - i + 2 + (j * 2)] = pyramidList.poll();

			}
		}
		return pyramid;
	}

	public static int[][] createPyramid(int n) throws CannotBuildPyramidException {
//find number of rows in pyramid
		int x = 0;
		for (int i = 1; x <= n; i++) {
			x += i;
			if (x == n) {
				x = i;
				break;
			}
			if (x > n)
				throw new CannotBuildPyramidException();

		}
		// find row length
		int y = x * 2 - 1;
		// build our pyramid and fill it with '0'
		int[][] pyramid = new int[x][y];
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				pyramid[i][j] = 0;

			}

		}
		return pyramid;

	}

}
