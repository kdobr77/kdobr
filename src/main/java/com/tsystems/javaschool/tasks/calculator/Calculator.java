package com.tsystems.javaschool.tasks.calculator;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

	/**
	 * Evaluate statement represented as string.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as
	 *                  decimal mark, parentheses, operations signs '+', '-', '*',
	 *                  '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return string value containing result of evaluation or null if statement is
	 *         invalid
	 */

	public String evaluate(String statement) {

		if (statement == null || statement.length() == 0
				|| statement.chars().anyMatch(c -> c < 40 || c > 57 || c == 44)) {
			return null;
		}
		if (statement.matches(".*[/\\+\\-\\*\\.]{2}.*")) {
			return null;
		}

		// Get an instance of JS Nashorn engine
				ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

		
		try {
			String result = engine.eval(statement).toString();
			if (result.toLowerCase().equals("infinity"))
				return null;
			return result;

		} catch (ScriptException ex) {
			return null;
		}

	}

}
