package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     * @throws Exception 
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
      
    	if (null == x || null == y) throw new IllegalArgumentException ();
    	 	
    	if (x.size()==0) return true;
		
    	List test =   (List) y.stream().filter(s -> x.contains(s)).distinct()
    			.collect(Collectors.toCollection(ArrayList::new));
    	
    	if (x.equals(test)) return true;
    	return false;
    }
}
